package edu.khtn.module03_bai09_service_broadcast_notify.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import edu.khtn.module03_bai09_service_broadcast_notify.MainActivity;
import edu.khtn.module03_bai09_service_broadcast_notify.R;

public class MyService extends Service {
    int res = 0;
    int id = 0;
    String msg = "";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    int edit1 = Integer.parseInt(intent.getStringExtra("edit1"));
                    int edit2 = Integer.parseInt(intent.getStringExtra("edit2"));
                    String editX = intent.getStringExtra("editX");
                    if (editX.contentEquals("+")) {
                        res = edit1 + edit2;
                        id = 1;
                        msg = "Result of: " + edit1 + " plus " + edit2;
                    } else if (editX.contentEquals("-")) {
                        res = edit1 - edit2;
                        id = 2;
                        msg = "Result of: " + edit1 + " minus " + edit2;
                    } else if (editX.contentEquals("*")) {
                        res = edit1 * edit2;
                        id = 3;
                        msg = "Result of: " + edit1 + " multiply " + edit2;
                    } else if (editX.contentEquals("/")) {
                        res = edit1 / edit2;
                        id = 4;
                        msg = "Result of: " + edit1 + " divide " + edit2;
                    } else {
                        msg = "Calculator failed ";
                    }
                    for (int i = 0; i < res; i++) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Progress is: " + i);
                    }
                } catch (Exception e) {
                    msg = "Calculator failed ";
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                try {
                    if (MainActivity.isResume == true) {
                        MainActivity.txtResult.setText(res + "");
                    } else {
                        postNotify(getBaseContext(), res + "", id, msg);
                    }
                } catch (Exception e) {
                    postNotify(getBaseContext(), res + "", id, msg);
                }
                super.onPostExecute(o);
            }
        };
        asyncTask.execute();
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void postNotify(Context context, String res, int id, String msg) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("result", res);
        PendingIntent pendingIntent = PendingIntent.getActivity
                (context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle("Result");
        builder.setContentText(msg);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setSound(sound);
        NotificationManager manager = (NotificationManager)
                context.getSystemService(NOTIFICATION_SERVICE);
        manager.notify(id, builder.build());
    }
}
