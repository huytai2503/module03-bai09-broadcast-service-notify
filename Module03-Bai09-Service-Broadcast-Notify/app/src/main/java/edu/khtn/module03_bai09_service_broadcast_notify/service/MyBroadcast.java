package edu.khtn.module03_bai09_service_broadcast_notify.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyBroadcast extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentService = new Intent(context, MyService.class);
        intentService.putExtras(intent);
        context.startService(intentService);
    }
}
