package edu.khtn.module03_bai09_service_broadcast_notify;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import edu.khtn.module03_bai09_service_broadcast_notify.service.MyBroadcast;

public class MainActivity extends AppCompatActivity {
    String mBROADCAST_KEY = "manual.receive.broadcast";
    MyBroadcast myBroadcast;
    public static boolean isResume;
    public static TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtResult = (TextView) findViewById(R.id.text_result);
        Button btnReg = (Button) findViewById(R.id.button_registry);
        Button btnUnreg = (Button) findViewById(R.id.button_unregistry);
        myBroadcast = new MyBroadcast();
        txtResult.setText(getIntent().getStringExtra("result"));

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentFilter intentFilter = new IntentFilter(mBROADCAST_KEY);
                registerReceiver(myBroadcast, intentFilter);
                showToast("Registry successed");
            }
        });

        btnUnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unregisterReceiver(myBroadcast);
                showToast("Unregistry successed");
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        txtResult.setText(intent.getStringExtra("result"));
    }

    @Override
    protected void onStop() {
        isResume = false;
        super.onStop();
    }

    @Override
    protected void onResume() {
        isResume = true;
        super.onResume();
    }

    public void showToast(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
