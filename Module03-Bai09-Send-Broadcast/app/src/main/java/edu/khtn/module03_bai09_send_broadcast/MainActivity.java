package edu.khtn.module03_bai09_send_broadcast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String mBROADCAST_KEY_1 = "manual.receive.broadcast";
    String mBROADCAST_KEY_2 = "auto.receive.broadcast";
    Button btnSend, btnSend2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSend = (Button) findViewById(R.id.button_send);
        btnSend2 = (Button) findViewById(R.id.button_send2);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send(mBROADCAST_KEY_1);
            }
        });

        btnSend2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send(mBROADCAST_KEY_2);
            }
        });
    }

    public void send(String broadcastKey){
        EditText edit1 = (EditText) findViewById(R.id.edit_1);
        EditText edit2 = (EditText) findViewById(R.id.edit_2);
        EditText editX = (EditText) findViewById(R.id.edit_X);
        Intent intent = new Intent(broadcastKey);
        intent.putExtra("edit1", edit1.getText().toString());
        intent.putExtra("edit2", edit2.getText().toString());
        intent.putExtra("editX", editX.getText().toString());
        sendBroadcast(intent);
        Toast.makeText(MainActivity.this, "Send success", Toast.LENGTH_SHORT).show();
    }
}
